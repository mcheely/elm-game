# Hybrid text adventure & side-scroller

The nice thing about this is that they could be written as two separate
games that we just stitch together in the DOM

## Text Adventure

If we want to use Elm for this, there's: https://package.elm-lang.org/packages/jschomay/elm-narrative-engine/latest/Engine

The story goes something like this:

Main character's brother/friend/uncle/third cousin liked to go backpacking
in a big, old forest. He disappeared a few weeks ago. There were search
parties, he was never found. The character has a dream where he's his
brother and is hiking in the forest. He sees something strange in the
woods and turns off the trail. Protagonist wakes up and can't shake the
dream, decides to go look.

Interactive play starts on the trail. Player can find the turn off, and it
leads to an old ruined temple, most of which is underground. There are
some simple puzzles. Lots of weird eldritch shit around, some of which is
presented as relevant to finding your sibling, some not. Interacting with
too much weird stuff eats away at the character's sanity. Some sanity loss
probably required to progress. (see sanity mechanics below)

Eventually the character finds a room with a strange artifact in it.
Finding this triggers an immediate threat of some sort - hideous
abominations rushing up from the depths of the temple. The artifact is
described in weird terms, but if you read closely between the lines, it's  
a description of a gamepad. When the character picks it up, the text
describes a door opening in his mind as he begins to perceive new and
impossible dimensions. (Possible option here - typing is too slow to pick
up the artifact in time, the player must actually pick up a gamepad and
press a button to trigger the transition). This triggers a transition to
the 2d runner, where the character must flee the creatures.

### Other endings

Not turning down the trail and just going back home: Nothing bad happens,
but there's a feeling that something is in the forest, watching.

Go to many levels deep in the temple: Get lost in the darkness and run out
of food and water. Start hearing things after the flashlight battery dies.

Run out of sanity: No matter what the player types, the game always takes
a different route until the player reaches a specific location and performs
some sort of horrific ritual to open a gate to another plane of reality.

## Runner mode

The character can speed up briefly, slow down briefly, duck/roll and jump
to avoid obstacles and monsters. There's no way to fight back. The main
pursuit is some sort of giant abomination that always pursues from the
left. If the abomination is on the screen, the player begins to lose
sanity. The closer they are to it, the more they lose. Collisions with
obstacles and monsters both slow the player, but monsters also reduce
sanity. Only goal is to make it to the end of the level.

## Sanity effects

Loss of sanity starts with cosmetic effects, eventually transitioning to
functional effects.

### Text mode sanity

_cosmetic effects_

- Letters get extra unicode decorations on them (but are still legible)
- Letters are offset in weird ways, and twitch on the screen

_functional effects_

- A different (creepy, nonsensical) text starts to bleed through the game text, first just a letter or two in a few places, then entire words.

### Runner mode sanity

_cosmetic effects_

- Weird shader voodoo

_functional effects_

- Button mappings start to change.
- Some enemies are illusory?
