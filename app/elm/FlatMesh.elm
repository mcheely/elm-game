module FlatMesh exposing (FlatMesh, createMesh)

import Math.Vector2 as Vec2 exposing (Vec2, vec2)
import Math.Vector3 as Vec3 exposing (Vec3, vec3)
import WebGL exposing (Mesh)
import WebGL.Texture as Texture exposing (Texture)


type alias FlatMesh =
    Mesh Vertex


type alias Vertex =
    { position : Vec3
    , coord : Vec2
    }


createMesh : ( Float, Float ) -> ( Float, Float ) -> Mesh Vertex
createMesh ( xSize, ySize ) ( xRatio, yRatio ) =
    let
        topLeft =
            Vertex (vec3 -xSize ySize 0) (vec2 0 yRatio)

        topRight =
            Vertex (vec3 xSize ySize 0) (vec2 xRatio yRatio)

        bottomLeft =
            Vertex (vec3 -xSize -ySize 0) (vec2 0 0)

        bottomRight =
            Vertex (vec3 xSize -ySize 0) (vec2 xRatio 0)
    in
    WebGL.triangles
        [ ( topLeft, topRight, bottomLeft )
        , ( bottomLeft, topRight, bottomRight )
        ]
