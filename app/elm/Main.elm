module Main exposing (main)

import Array
import Background exposing (Background, Layer(..))
import Browser exposing (Document)
import Browser.Events exposing (onAnimationFrameDelta)
import Entity exposing (Entity)
import Html exposing (Html, text)
import Html.Attributes exposing (height, style, width)
import Screen exposing (Screen, screen, toHtml)
import Sprite exposing (Sprite)
import Task
import WebGL.Texture as Texture


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }



-- Model


type Model
    = Loading
    | LoadFailure Texture.Error
    | Ready GameModel


type alias GameModel =
    { background : Background
    , bgOffset : Float
    , frontLiver : Entity
    , backLiver : Entity
    , player : Entity
    }


init : () -> ( Model, Cmd Msg )
init _ =
    ( Loading
    , loadTextures
    )


initGameModel : Background -> Sprite -> Sprite -> GameModel
initGameModel background sprite playerSprite =
    { background = background
    , bgOffset = 0.0
    , frontLiver = Entity sprite ( 0.5, -0.4 )
    , backLiver = Entity sprite ( -0.5, -0.3 )
    , player = Entity playerSprite ( 0.0, -0.67 )
    }


ground =
    [ Layer
        { texture = "background/Layer_0010_1.png"
        , parallax = 0.55
        }
    , Layer
        { texture = "background/Layer_0009_2.png"
        , parallax = 0.6
        }
    , Layer
        { texture = "background/Layer_0008_3.png"
        , parallax = 0.65
        }
    , Layer
        { texture = "background/Layer_0007_Lights.png"
        , parallax = 0.7
        }
    , Layer
        { texture = "background/Layer_0006_4.png"
        , parallax = 0.75
        }
    , Layer
        { texture = "background/Layer_0005_5.png"
        , parallax = 0.8
        }
    , Layer
        { texture = "background/Layer_0004_Lights.png"
        , parallax = 0.85
        }
    , Slot { parallax = 0.875 }
    , Layer
        { texture = "background/Layer_0003_6.png"
        , parallax = 0.9
        }
    , Layer
        { texture = "background/Layer_0002_7.png"
        , parallax = 0.95
        }
    , Layer
        { texture = "background/Layer_0001_8.png"
        , parallax = 1.0
        }
    , Slot { parallax = 1.0 }
    , Layer
        { texture = "background/Layer_0000_9.png"
        , parallax = 1.05
        }
    ]


loadTextures : Cmd Msg
loadTextures =
    Task.map3 initGameModel
        (Background.load ground)
        (Sprite.load gameScreen "eldritch_liver.png")
        (Sprite.load gameScreen "character/Skeleton Walk.png")
        |> Task.attempt TextureLoad



-- Update


type Msg
    = Frame Float
    | TextureLoad (Result Texture.Error GameModel)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        TextureLoad loadResult ->
            case loadResult of
                Ok gameModel ->
                    ( Ready gameModel, Cmd.none )

                Err error ->
                    ( LoadFailure error, Cmd.none )

        Frame delta ->
            case model of
                Ready gameModel ->
                    ( Ready
                        { gameModel
                            | bgOffset =
                                gameModel.bgOffset + (delta / 10000)
                        }
                    , Cmd.none
                    )

                _ ->
                    ( model, Cmd.none )



-- View


gameScreen : Screen
gameScreen =
    screen 1024 640


view : Model -> Html Msg
view model =
    case model of
        Loading ->
            text "Loading..."

        LoadFailure errorTexture ->
            text "Failed to load texture"

        Ready gameModel ->
            Screen.toHtml gameScreen
                (Background.render
                    gameScreen
                    gameModel.background
                    gameModel.bgOffset
                    (Array.fromList
                        [ [ gameModel.frontLiver, gameModel.player ]
                        , [ gameModel.backLiver ]
                        ]
                    )
                )


document : Model -> Document Msg
document model =
    { title = "Game Exploration"
    , body =
        [ view model
        ]
    }


subscriptions : Model -> Sub Msg
subscriptions model =
    onAnimationFrameDelta Frame
