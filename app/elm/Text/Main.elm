module Text.Main exposing (main)

import Browser
import Dict exposing (Dict)
import Engine exposing (Rule, currentLocationIs, withAnyItem)
import Html exposing (Html, button, div, input, text)
import Html.Attributes exposing (id, max, min, type_, value)
import Html.Events exposing (onInput)
import Random exposing (Seed)
import Task
import Text.Sanity exposing (crazyText)
import Time


main =
    Browser.element
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


type alias Model =
    { storyModel : Engine.Model
    , sanity : Int
    , seed : Seed
    }


init : () -> ( Model, Cmd Msg )
init _ =
    ( { sanity = 100
      , storyModel = initialStoryModel
      , seed = Random.initialSeed 1234
      }
    , Task.map Time.posixToMillis Time.now
        |> Task.map Random.initialSeed
        |> Task.perform GeneratedSeed
    )


initialStoryModel : Engine.Model
initialStoryModel =
    Engine.init
        { items = []
        , locations = [ "Home" ]
        , characters = []
        }
        rules


rules : Dict String Rule
rules =
    Dict.fromList
        [ ( "test"
          , { interaction = withAnyItem
            , conditions = [ currentLocationIs "Home" ]
            , changes = []
            }
          )
        ]



-- update


type Msg
    = StoryAction String
    | SetSanity String
    | GeneratedSeed Seed


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        StoryAction id ->
            let
                ( newStoryModel, rule ) =
                    Engine.update id model.storyModel
            in
            ( { model | storyModel = newStoryModel }, Cmd.none )

        GeneratedSeed seed ->
            ( { model | seed = seed }, Cmd.none )

        SetSanity sanityStr ->
            ( { model
                | sanity =
                    String.toInt sanityStr
                        |> Maybe.withDefault model.sanity
              }
            , Cmd.none
            )



-- View


view : Model -> Html Msg
view model =
    let
        sanityFactor =
            toFloat (100 - model.sanity) / 100.0

        textGen =
            crazyText sanityFactor loremIpsum
    in
    div [ id "mainText" ]
        [ sanitySlider model.sanity
        , div [] (Random.step textGen model.seed |> Tuple.first)
        ]


sanitySlider : Int -> Html Msg
sanitySlider sanity =
    div [ id "sanitySlider" ]
        [ input
            [ type_ "range"
            , min "0"
            , max "100"
            , value (String.fromInt sanity)
            , onInput SetSanity
            ]
            []
        ]


loremIpsum =
    """
The bureaucrat fell from the sky.

For an instant Miranda lay blue and white beneath him, the icecaps fat and ready to melt, and then he was down. He took a highspeed across the stony plains of the Piedmont to the heliostat terminus at Port Richmond, and caught the first flight out. The airship Leviathan lofted him across the fall line and over the forests and coral hills of the Tidewater. Specialized ecologies were astir there, preparing for the transforming magic of the jubilee tides. In ramshackle villages and hidden plantations people made their varied provisions for the evacuation.

The Leviathan's lounge was deserted. Hands clasped behind him, the bureaucrat stared moodily out the stern windows. The Piedmont was dim and blue, a storm front on the horizon. He imagined the falls, where fish-hawks hovered on rising thermals and the river Noon cascaded down and lost its name. Below, the Tidewater swarmed with life, like blue-green mold growing magnified in a petri dish. The thought of all the mud and poverty down there depressed him. He yearned for the cool, sterile environments of deep space.
"""


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none
