module Text.Sanity exposing (crazyText, zalgoText)

import Array exposing (Array)
import Html exposing (Html, span, text)
import Html.Attributes exposing (class, style)
import Random exposing (Generator, Seed)
import Random.Extra as Random
import Random.List exposing (choose)


crazyText : Float -> String -> Generator (List (Html msg))
crazyText insanity words =
    let
        wiggleFactor =
            insanity

        zalgoFactor =
            clamp 0 1 ((3 / 2) * (insanity - 0.33))

        rambleFactor =
            clamp 0 1 (3 * (insanity - 0.66))
    in
    String.toList words
        |> List.indexedMap (maybeRamble rambleFactor)
        |> Random.combine
        |> Random.andThen
            (\chars ->
                List.map
                    (madnessSpan zalgoFactor wiggleFactor)
                    chars
                    |> Random.combine
            )


madnessSpan : Float -> Float -> MadnessChar -> Generator (Html msg)
madnessSpan zalgoFactor wiggleFactor madChar =
    case madChar of
        Rambling char ->
            Random.constant (rambleSpan char)

        Original char ->
            crazySpan zalgoFactor wiggleFactor char


rambleSpan : Char -> Html msg
rambleSpan char =
    span [ class "mad-rambling" ] [ text (String.fromChar char) ]


maybeRamble : Float -> Int -> Char -> Generator MadnessChar
maybeRamble rambleFactor idx original =
    let
        rambleIdx =
            modBy (Array.length rambleArray) idx

        rambleChar =
            Array.get rambleIdx rambleArray
                |> Maybe.withDefault original
    in
    withFrequency rambleFactor
        (Random.constant (Rambling rambleChar))
        (Original original)


type MadnessChar
    = Rambling Char
    | Original Char


madRamblings : String
madRamblings =
    """
Yog-Sothoth knows the gate. Yog-Sothoth is the gate. Yog-Sothoth is the key and guardian of the gate. Past, present, future, all are one in Yog-Sothoth.
"""


rambleArray : Array Char
rambleArray =
    Array.fromList (String.toList madRamblings)


crazySpan : Float -> Float -> Char -> Generator (Html msg)
crazySpan zalgoFactor wiggleFactor char =
    zalgoText zalgoFactor char
        |> Random.andThen (wiggleSpan wiggleFactor)


wiggleSpan : Float -> String -> Generator (Html msg)
wiggleSpan wiggleFactor text =
    let
        offsetMax =
            0.3

        offsetGen =
            withFrequency wiggleFactor
                (Random.float -offsetMax offsetMax)
                0
    in
    Random.map (offsetSpan text) offsetGen


offsetSpan : String -> Float -> Html msg
offsetSpan str offset =
    span
        [ style "position" "relative"
        , style "top" (String.fromFloat offset ++ "ch")
        ]
        [ text str ]


withFrequency : Float -> Generator a -> a -> Generator a
withFrequency freq gen constant =
    Random.frequency
        ( 1 - freq, Random.constant constant )
        [ ( freq, gen ) ]


zalgoMax : Int
zalgoMax =
    6


zalgoText : Float -> Char -> Generator String
zalgoText zalgoFactor character =
    let
        maxZalgos =
            ceiling (zalgoFactor * toFloat zalgoMax)

        genZalgos =
            withFrequency zalgoFactor
                (Random.int 1 maxZalgos
                    |> Random.andThen (getSomeZalgos allZalgo)
                )
                []
    in
    Random.map2 addZalgos
        (Random.constant character)
        genZalgos


addZalgos : Char -> List Char -> String
addZalgos char zalgos =
    if char /= ' ' then
        char
            :: zalgos
            |> String.fromList

    else
        String.fromChar char


getSomeZalgos : List Char -> Int -> Generator (List Char)
getSomeZalgos zalgos length =
    getMoreZalgos length ( [], zalgos )


getMoreZalgos : Int -> ( List Char, List Char ) -> Generator (List Char)
getMoreZalgos length ( zalgos, remainingZalgos ) =
    case length of
        0 ->
            Random.constant zalgos

        _ ->
            choose remainingZalgos
                |> Random.andThen
                    (\( selected, remaining ) ->
                        case selected of
                            Nothing ->
                                Random.constant zalgos

                            Just zalgo ->
                                getMoreZalgos (length - 1) ( zalgo :: zalgos, remaining )
                    )


allZalgo =
    zalgoUp ++ zalgoLow


zalgoUp =
    [ '̍'
    , '̎'
    , '̄'
    , '̅'
    , '̿'
    , '̑'
    , '̆'
    , '̐'
    , '͒'
    , '͗'
    , '͑'
    , '̇'
    , '̈'
    , '̊'
    , '͂'
    , '̓'
    , '̈'
    , '͊'
    , '͋'
    , '͌'
    , '̃'
    , '̂'
    , '̌'
    , '͐'
    , '̀'
    , '́'
    , '̋'
    , '̏'
    , '̒'
    , '̓'
    , '̔'
    , '̽'
    , '̉'
    , 'ͣ'
    , 'ͤ'
    , 'ͥ'
    , 'ͦ'
    , 'ͧ'
    , 'ͨ'
    , 'ͩ'
    , 'ͪ'
    , 'ͫ'
    , 'ͬ'
    , 'ͭ'
    , 'ͮ'
    , 'ͯ'
    , '̾'
    , '͛'
    , '͆'
    , '̚'
    ]


zalgoMid =
    [ '̕'
    , '̛'
    , '̀'
    , '́'
    , '͘'
    , '̡'
    , '̢'
    , '̧'
    , '̨'
    , '̴'
    , '̵'
    , '̶'
    , '͜'
    , '͝'
    , '͞'
    , '͟'
    , '͠'
    , '͢'
    , '̸'
    , '̷'
    , '͡'
    , '҉'
    ]


zalgoLow =
    [ '̖'
    , '̗'
    , '̘'
    , '̙'
    , '̜'
    , '̝'
    , '̞'
    , '̟'
    , '̠'
    , '̤'
    , '̥'
    , '̦'
    , '̩'
    , '̪'
    , '̫'
    , '̬'
    , '̭'
    , '̮'
    , '̯'
    , '̰'
    , '̱'
    , '̲'
    , '̳'
    , '̹'
    , '̺'
    , '̻'
    , '̼'
    , 'ͅ'
    , '͇'
    , '͈'
    , '͉'
    , '͍'
    , '͎'
    , '͓'
    , '͔'
    , '͕'
    , '͖'
    , '͙'
    , '͚'
    , '̣'
    ]
