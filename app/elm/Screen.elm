module Screen exposing (Screen, backgroundMesh, fractionalSizeOf, screen, toHtml)

import FlatMesh exposing (FlatMesh)
import Html exposing (Html)
import Html.Attributes exposing (height, style, width)
import Math.Vector2 as Vec2 exposing (Vec2, vec2)
import Math.Vector3 as Vec3 exposing (Vec3, vec3)
import WebGL exposing (Mesh)


type Screen
    = Screen Viewport


screen : Int -> Int -> Screen
screen width height =
    Screen
        { width = width
        , height = height
        , mesh = FlatMesh.createMesh ( 1, 1 ) (textureClip width height)
        }


textureClip width height =
    case compare width height of
        EQ ->
            ( 1, 1 )

        GT ->
            ( 1, toFloat height / toFloat width )

        LT ->
            ( toFloat width / toFloat height, 1 )


backgroundMesh : Screen -> FlatMesh
backgroundMesh screen_ =
    case screen_ of
        Screen viewport ->
            viewport.mesh


fractionalSizeOf : ( Int, Int ) -> Screen -> ( Float, Float )
fractionalSizeOf ( width, height ) screen_ =
    let
        ( screenWidth, screenHeight ) =
            size screen_
    in
    ( toFloat width / toFloat screenWidth, toFloat height / toFloat screenHeight )


toHtml : Screen -> List WebGL.Entity -> Html msg
toHtml screen_ entities =
    case screen_ of
        Screen viewport ->
            WebGL.toHtmlWith
                [ WebGL.alpha True ]
                [ width viewport.width
                , height viewport.height
                ]
                entities


size : Screen -> ( Int, Int )
size screen_ =
    case screen_ of
        Screen viewport ->
            ( viewport.width, viewport.height )


type alias Viewport =
    { width : Int
    , height : Int
    , mesh : FlatMesh
    }
