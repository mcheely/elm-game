module Sprite exposing (Sprite, load, render, renderAt)

import FlatMesh exposing (FlatMesh)
import Render
import Screen exposing (Screen)
import Task exposing (Task)
import WebGL
import WebGL.Texture as Texture exposing (Texture)


type alias SpriteInfo =
    { texture : Texture
    , mesh : FlatMesh
    }


type Sprite
    = Sprite SpriteInfo


load : Screen -> String -> Task Texture.Error Sprite
load screen url =
    Texture.load url
        |> Task.map (fromTexture screen)


fromTexture : Screen -> Texture -> Sprite
fromTexture screen texture =
    let
        pixelSize =
            Texture.size texture
    in
    Sprite
        { texture = texture
        , mesh =
            FlatMesh.createMesh
                (Screen.fractionalSizeOf pixelSize screen)
                ( 1, 1 )
        }


render : Sprite -> WebGL.Entity
render sprite =
    case sprite of
        Sprite spriteInfo ->
            Render.render
                spriteInfo.mesh
                spriteInfo.texture
                ( 0.0, 0.0 )
                ( 0.0, 0.0 )


renderAt : ( Float, Float ) -> Sprite -> WebGL.Entity
renderAt position sprite =
    case sprite of
        Sprite spriteInfo ->
            Render.render
                spriteInfo.mesh
                spriteInfo.texture
                position
                ( 0.0, 0.0 )
