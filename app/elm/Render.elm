module Render exposing (render)

import FlatMesh exposing (FlatMesh)
import Math.Matrix4 as Mat4 exposing (Mat4)
import Math.Vector2 as Vec2 exposing (Vec2, vec2)
import Math.Vector3 as Vec3 exposing (Vec3, vec3)
import WebGL exposing (Shader)
import WebGL.Settings exposing (Setting)
import WebGL.Settings.Blend as Blend
import WebGL.Texture as Texture exposing (Texture)


render :
    FlatMesh
    -> Texture
    -> ( Float, Float )
    -> ( Float, Float )
    -> WebGL.Entity
render mesh texture ( posX, posY ) ( texOffsetX, texOffsetY ) =
    WebGL.entityWith
        [ alphaBlend ]
        vertexShader
        fragmentShader
        mesh
        { img = texture
        , perspective = flatPerspective
        , placement = vec2 posX posY
        , textureOffset = vec2 texOffsetX texOffsetY
        }


alphaBlend : Setting
alphaBlend =
    Blend.custom
        { r = 0
        , g = 0
        , b = 0
        , a = 0
        , color = Blend.customAdd Blend.srcAlpha Blend.oneMinusSrcAlpha
        , alpha = Blend.customAdd Blend.one Blend.oneMinusSrcAlpha
        }


type alias Vertex =
    { position : Vec3
    , coord : Vec2
    }


type alias Uniforms =
    { img : Texture
    , perspective : Mat4
    , placement : Vec2
    , textureOffset : Vec2
    }


flatPerspective : Mat4
flatPerspective =
    let
        eye =
            vec3 0 0 1
                |> Vec3.normalize
                |> Vec3.scale 1
    in
    Mat4.mul
        (Mat4.makePerspective 90 1 0.01 100)
        (Mat4.makeLookAt
            eye
            (vec3 0 0 0)
            (vec3 0 1 0)
        )


vertexShader : Shader Vertex Uniforms { vcoord : Vec2 }
vertexShader =
    [glsl|
      attribute vec3 position;
      attribute vec2 coord;
      uniform mat4 perspective;
      uniform vec2 placement;
      varying vec2 vcoord;

      void main() {
        gl_Position = perspective * vec4(position, 1.0)
                    + vec4(placement, 0.0, 0.0);
        vcoord = coord.xy;
      }
    |]


fragmentShader : Shader {} Uniforms { vcoord : Vec2 }
fragmentShader =
    [glsl|
      precision mediump float;
      uniform sampler2D img;
      varying vec2 vcoord;
      uniform vec2 textureOffset;

      void main() {
        gl_FragColor = texture2D(img, vcoord + textureOffset);
      }
    |]
