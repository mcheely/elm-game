module Entity exposing (Entity, render)

import Sprite exposing (Sprite)
import WebGL


type alias Entity =
    { sprite : Sprite
    , position : ( Float, Float )
    }


render : Entity -> WebGL.Entity
render entity =
    Sprite.renderAt entity.position entity.sprite
