module Background exposing (Background, Layer(..), load, render)

import Array exposing (Array)
import Entity exposing (Entity)
import FlatMesh
import Render
import Screen exposing (Screen)
import Sprite exposing (Sprite)
import Task exposing (Task)
import WebGL
import WebGL.Texture as Texture exposing (Texture)


type Layer
    = Slot { parallax : Float }
    | Layer
        { texture : String
        , parallax : Float
        }


type Background
    = Background (List RenderLayer)


type RenderLayer
    = RenderSlot Int Float
    | RenderLayer
        { texture : Texture
        , parallax : Float
        }


load : List Layer -> Task Texture.Error Background
load layers =
    List.foldr loadLayer ( [], 0 ) layers
        |> Tuple.first
        |> Task.sequence
        |> Task.map Background


loadLayer :
    Layer
    -> ( List (Task Texture.Error RenderLayer), Int )
    -> ( List (Task Texture.Error RenderLayer), Int )
loadLayer layer ( tasks, slotIndex ) =
    let
        ( task, newIndex ) =
            case layer of
                Slot slot ->
                    ( Task.succeed (RenderSlot slotIndex slot.parallax)
                    , slotIndex + 1
                    )

                Layer ground ->
                    ( Texture.load ground.texture
                        |> Task.map
                            (\tex ->
                                RenderLayer
                                    { texture = tex
                                    , parallax = ground.parallax
                                    }
                            )
                    , slotIndex
                    )
    in
    ( task :: tasks, newIndex )


render :
    Screen
    -> Background
    -> Float
    -> Array (List Entity)
    -> List WebGL.Entity
render screen background offset entities =
    case background of
        Background layers ->
            List.map (renderLayer screen offset entities) layers
                |> List.concat


renderLayer :
    Screen
    -> Float
    -> Array (List Entity)
    -> RenderLayer
    -> List WebGL.Entity
renderLayer screen offset allEntities layer =
    case layer of
        RenderLayer ground ->
            [ renderGround screen offset ground ]

        RenderSlot pos parallax ->
            let
                entities =
                    Array.get pos allEntities
                        |> Maybe.withDefault []
            in
            renderEntities parallax entities


renderGround screen offset ground =
    Render.render
        (Screen.backgroundMesh screen)
        ground.texture
        ( 0.0, 0.0 )
        ( offset * ground.parallax, 0.0 )


renderEntities : Float -> List Entity -> List WebGL.Entity
renderEntities parallax entities =
    List.map Entity.render entities
