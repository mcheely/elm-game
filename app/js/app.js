import { Elm as Scroller } from "../elm/Main.elm";
import { Elm as Story } from "../elm/Text/Main.elm";

const scrollerId = "scroller";
const storyId = "story";

Scroller.Main.init({
  node: document.getElementById("scroller")
});

Story.Text.Main.init({
  node: document.getElementById("story")
});
